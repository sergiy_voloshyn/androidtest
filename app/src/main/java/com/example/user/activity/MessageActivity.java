package com.example.user.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.content.SharedPreferences;
import android.content.Context;
import android.widget.EditText;
import android.app.Activity;


public class MessageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_message);
    }

    //@Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);

        String s1 = sharedPref.getString("testString", "onResume");
        int d1 = sharedPref.getInt("testInt", 0);

        EditText edit1 = (EditText) findViewById(R.id.editText1);
        //edit1.

        edit1.setText("");
        edit1.append(s1 + "\n");
        edit1.append(Integer.toString(d1) + "\n");


    }

    protected void onStop() {
        //message
        //run
        super.onStop();

        SharedPreferences pref = this.getPreferences(Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = pref.edit();

        String s1 = "  stop is ok";
        editor.putString("testString", s1);

        int d1 = 2572;

        editor.putInt("testInt", d1);

        editor.apply();//


    }

    protected void onPause() {
        super.onPause();

        SharedPreferences pref = this.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        String s1 = "onPause is ok";
        editor.putString("testString", s1);

        int d1 = 787;
        editor.putInt("testInt", d1);

        editor.apply();//

    }


}
