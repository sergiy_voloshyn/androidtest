package com.example.user.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
    }

    public void sendMessage1(View v) {
        //message

        switch (v.getId()) {
            case R.id.button1:
                Intent intent = new Intent(this, MessageActivity.class);
                startActivity(intent);
                // TODO Call second activity
                break;


            case R.id.button2:
                Intent intent2 = new Intent(this, StopActivity.class);
                startActivity(intent2);
                // TODO Call second activity
                break;
            default:
                break;
        }


        //EditText editText = (EditText) findViewById(R.id.edit_message);
        //String message = editText.getText().toString();
        //intent.putExtra(EXTRA_MESSAGE, message);
    }


}
